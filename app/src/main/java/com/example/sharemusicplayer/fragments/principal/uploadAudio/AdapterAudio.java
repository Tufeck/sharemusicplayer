package com.example.sharemusicplayer.fragments.principal.uploadAudio;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.sharemusicplayer.fragments.principal.uploadAudio.dialog.FragmentExternalAudio;
import com.example.sharemusicplayer.fragments.principal.uploadAudio.dialog.FragmentInternalAudio;


public class AdapterAudio extends FragmentPagerAdapter {
    private Context context;

    public AdapterAudio(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
    }

    @Override
    public Fragment getItem(int position) {
        if(position==0) return new FragmentInternalAudio(context);
        else if(position==1)return new FragmentExternalAudio(context);
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if(position==0) return "Interno";
        else if(position==1)return "SD Card";
        return null;
    }
}

