package com.example.sharemusicplayer.fragments.principal.perfil;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sharemusicplayer.classes.User;
import com.example.sharemusicplayer.fragments.principal.ComunicadorPrincipal;
import com.example.sharemusicplayer.fragments.principal.uploadAudio.dialog.WaitDialog;
import com.example.sharemusicplayer.Preferences;
import com.example.sharemusicplayer.R;
import com.example.sharemusicplayer.classes.Post;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.Date;


public class FragmentProfile extends Fragment {


    private TextView nombre,sex,instrumento,pais;
    private TextInputLayout textPost;
    private Button publicar;
    private ComunicadorPrincipal comunicadorPrincipal;
    private String username;
    private FirebaseFirestore database;
    private DialogFragment alertDialogProfile;

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_profile,container,false);
        database = FirebaseFirestore.getInstance();
        username = Preferences.obtenerPreferenceString(getContext(),Preferences.PREFERENCE_USUARIO_LOGIN);
        comunicadorPrincipal = (ComunicadorPrincipal) getActivity();



        nombre = (TextView) v.findViewById(R.id.nombre);
        instrumento = (TextView) v.findViewById(R.id.instrumento);
        sex = (TextView) v.findViewById(R.id.sex);
        pais = (TextView) v.findViewById(R.id.pais);
        publicar = (Button) v.findViewById(R.id.publicar);
        textPost = (TextInputLayout) v.findViewById(R.id.post);
        if (alertDialogProfile != null) {
            alertDialogProfile.dismiss();
            alertDialogProfile = null;
        }else{
            showWait();
        }
        CollectionReference ref = database.collection("usuarios");
        ref.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    for (QueryDocumentSnapshot document : task.getResult()) {
                        final User user = document.toObject(User.class);
                        String id = user.getIdUser();
                        if (id.equals(username)) {
                            nombre.setText(user.getName());
                            instrumento.setText(user.getInstrument());
                            if(user.getSex().equals("hombre")){
                                sex.setText("Hombre");
                            }else{
                                sex.setText("Mujer");
                            }
                            if (alertDialogProfile != null) {
                                alertDialogProfile.dismiss();
                                alertDialogProfile = null;
                            }

                            pais.setText(user.getCountry());

                        }
                    }
                }else{
                    Log.i("obtenerUsuario", "No se ha podido obtener usuario");
                }
            }
        }).addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (alertDialogProfile != null) {
                    alertDialogProfile.dismiss();
                    alertDialogProfile = null;
                }
            }
        });

        publicar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String fileName = System.currentTimeMillis()+"";
                if(validarTexto(textPost,textPost.getEditText().getText().toString())){
                    CharSequence s  = DateFormat.format("dd-MM-yyyy hh:mm:ss",  new Date().getTime());
                    Post p = new Post(username,textPost.getEditText().getText().toString(),"1",""+s,"","","");
                    p.setPostId(fileName);
                    if (alertDialogProfile != null) {
                        alertDialogProfile.dismiss();
                        alertDialogProfile = null;
                    }else{
                        showWait();
                    }
                    //FragmentPost.agregarPost(p,getContext(), user.getName()+" "+user.getLastname());
                    final CollectionReference ref = database.collection("Posts");
                    ref.document(fileName).set(p).addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            Toast toast = Toast.makeText(getContext(), "Post subido", Toast.LENGTH_LONG);
                            toast.show();
                            Log.i("Post", "post subido al firebase");
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.i("Post", e.getMessage());
                            Toast toast = Toast.makeText(getContext(), "Error al subir post", Toast.LENGTH_LONG);
                            toast.show();
                        }
                    });
                    textPost.getEditText().setText("");
                    if (alertDialogProfile != null) {
                        alertDialogProfile.dismiss();
                        alertDialogProfile = null;
                    }
                    comunicadorPrincipal.actualizarPost();
                    comunicadorPrincipal.hideKeyboard();
                }
            }
        });

        return v;
    }

    private boolean validarTexto(TextInputLayout e,String s){
        if ((s.trim()).isEmpty()) {
            e.setError("campo invalido");
            return false;
        } else{
            e.setError(null);
            return true;
        }
    }

    private void showWait() {
        alertDialogProfile = new DialogFragment();

        alertDialogProfile.setCancelable(false);
        alertDialogProfile.setStyle(R.layout.dialog_wait, R.style.Theme_AppCompat_Dialog_Alert);
        alertDialogProfile.show(getFragmentManager(), "dfProfile");
    }
}
