package com.example.sharemusicplayer.fragments.principal.uploadAudio.dialog;

import android.app.AlertDialog;
import android.content.Context;

public class WaitDialog {
    public static AlertDialog alertDialog;

    public static void showWait(Context context, String message){
        alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle("");
        alertDialog.setMessage(message);
        alertDialog.setCancelable(false);
        alertDialog.show();
    }

    public static void closeWait(){
        alertDialog.dismiss();
        alertDialog=null;

    }
}
