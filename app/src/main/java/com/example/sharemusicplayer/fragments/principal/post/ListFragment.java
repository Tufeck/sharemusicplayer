package com.example.sharemusicplayer.fragments.principal.post;

import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.example.sharemusicplayer.activity.Login;
import com.example.sharemusicplayer.fragments.principal.uploadAudio.dialog.AudioAtributos;
import com.example.sharemusicplayer.fragments.principal.uploadAudio.dialog.DialogFragmentSelectAudio;
import com.example.sharemusicplayer.Preferences;
import com.example.sharemusicplayer.R;
import com.example.sharemusicplayer.classes.Post;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;


public class ListFragment extends Fragment {
    private List<AudioAtributos> atributosList;
    private RecyclerView rv;
    private ListAdapter adapterAudio;
    private LinearLayout layoutSinMusic;
    private FirebaseFirestore database;
    private FirebaseStorage firebaseStorage;
    private Button playList;
    private Button playRandom;
    public static boolean isPlayList = false;
    private List<AudioAtributos> copy;
    private DialogFragment alertDialogList;
    private LinearLayoutManager linearLayoutManager;
    private boolean pause = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list, container, false);
        atributosList = new ArrayList<>();
        final String username = Preferences.obtenerPreferenceString(getContext(), Preferences.PREFERENCE_USUARIO_LOGIN);
        database = FirebaseFirestore.getInstance();
        firebaseStorage = FirebaseStorage.getInstance();
        rv = (RecyclerView) view.findViewById(R.id.recyclerViewAudio);
        layoutSinMusic = (LinearLayout) view.findViewById(R.id.layoutVacioSinMusica);
        playList = view.findViewById(R.id.playList);
        playRandom = view.findViewById(R.id.playRandom);
        linearLayoutManager =
                new LinearLayoutManager(getActivity());
        linearLayoutManager.setStackFromEnd(true);
        linearLayoutManager.setReverseLayout(true);
        rv.setLayoutManager(linearLayoutManager);
        copy = new ArrayList<>();


        adapterAudio = new ListAdapter(atributosList, getContext(), (DialogFragmentSelectAudio) getParentFragment());
        rv.setAdapter(adapterAudio);
        if (alertDialogList == null) {
            showWait();

        } else {
            alertDialogList.dismiss();
            alertDialogList = null;
        }
        final CollectionReference ref = database.collection("Posts");
        ref.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    boolean noData = true;
                    for (QueryDocumentSnapshot document : task.getResult()) {

                        Post post = document.toObject(Post.class);
                        if (post.getIdUser().equals(username) && post.getType().equals("2")) {
                            AudioAtributos audioAtributos = new AudioAtributos();
                            audioAtributos.setNameSong(post.getNombreMusic());
                            audioAtributos.setDuracion(post.getMusciDuration());
                            audioAtributos.setData(post.getDataMusic());
                            audioAtributos.setPlay(false);
                            addMusic(audioAtributos);
                            noData = false;
                        }


                    }
                    if (alertDialogList != null) {
                        alertDialogList.dismiss();
                        alertDialogList = null;
                    }
                    if (noData) {
                        layoutSinMusic.setVisibility(View.VISIBLE);
                    }
                } else {
                    if (alertDialogList != null) {
                        alertDialogList.dismiss();
                        alertDialogList = null;
                    }
                    Log.i("Task fail", "no se han podido cargar los usuarios");
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.i("Task fail", e.getMessage());
                if (alertDialogList != null) {
                    alertDialogList.dismiss();
                    alertDialogList = null;
                }
            }
        }).addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                playList.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        isPlayList = true;
                        if (copy.size() > 0) {
                            atributosList = copy;
                            adapterAudio.notifyDataSetChanged();
                        }
                        for (int i = 0; i < atributosList.size(); i++) {
                            if (atributosList.get(i).getMediaPlayer() != null) {
                                atributosList.get(i).getMediaPlayer().stop();
                                atributosList.get(i).getMediaPlayer().release();
                                atributosList.get(i).setMediaPlayer(null);
                            }
                            atributosList.get(i).setPlay(false);
                            adapterAudio.notifyDataSetChanged();
                            adapterAudio.notifyItemChanged(i);
                        }

                        for (int i = atributosList.size() - 1; i >= 0; i--) {
                            if (i == atributosList.size() - 1) {
                                play(i, false);
                            }
                        }

                    }
                });

                playRandom.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        isPlayList = true;


                        for (int i = 0; i < atributosList.size(); i++) {
                            if (atributosList.get(i).getMediaPlayer() != null) {
                                atributosList.get(i).getMediaPlayer().stop();
                                atributosList.get(i).getMediaPlayer().release();
                                atributosList.get(i).setMediaPlayer(null);
                            }
                            atributosList.get(i).setPlay(false);
                            adapterAudio.notifyDataSetChanged();
                            adapterAudio.notifyItemChanged(i);
                        }
                        Random random = new Random();
                        if (!atributosList.isEmpty()) {
                            int rand = random.nextInt(atributosList.size());
                            play(rand, true);
                        }


                    }
                });
            }
        });

        return view;
    }

    private void addMusic(AudioAtributos audioAtributos) {
        atributosList.add(audioAtributos);
        adapterAudio.notifyDataSetChanged();
    }

    private void play(final int aux, final boolean random) {
        Log.i("auxiliar", aux + "");
        if (atributosList.get(aux).getMediaPlayer() == null) {
            linearLayoutManager.scrollToPosition(aux);
            if (alertDialogList == null) {
                showWait();
            } else {
                alertDialogList.dismiss();
                alertDialogList = null;
            }
            StorageReference storageRef = firebaseStorage.getReference();
            storageRef.child(atributosList.get(aux).getData()).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                @Override
                public void onSuccess(Uri uri) {
                    try {
                        if (!pause) {
                            atributosList.get(aux).setMediaPlayer(new MediaPlayer());
                            atributosList.get(aux).getMediaPlayer().setDataSource(uri.toString());
                            atributosList.get(aux).getMediaPlayer().setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                                @Override
                                public void onPrepared(MediaPlayer mp) {

                                    atributosList.get(aux).getMediaPlayer().start();
                                    atributosList.get(aux).setPlay(true);
                                    adapterAudio.notifyDataSetChanged();
                                    adapterAudio.notifyItemChanged(aux);
                                    if (alertDialogList != null) {
                                        alertDialogList.dismiss();
                                        alertDialogList = null;
                                    }
                                }
                            });

                            atributosList.get(aux).getMediaPlayer().prepareAsync();
                            atributosList.get(aux).getMediaPlayer().setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                                @Override
                                public void onCompletion(MediaPlayer mp) {
                                    atributosList.get(aux).getMediaPlayer().stop();
                                    atributosList.get(aux).getMediaPlayer().release();
                                    atributosList.get(aux).setMediaPlayer(null);

                                    atributosList.get(aux).setPlay(false);
                                    adapterAudio.notifyDataSetChanged();
                                    adapterAudio.notifyItemChanged(aux);
                                    if (!random) {
                                        if (aux > 0) {
                                            play(aux - 1, false);
                                        }else{
                                            isPlayList = false;
                                        }
                                    }else {
                                        Random rand = new Random();
                                        int ran = rand.nextInt(atributosList.size());
                                        play(ran, true);
                                    }
                                }
                            });
                        }
                    } catch (IOException e) {
                        if (alertDialogList != null) {
                            alertDialogList.dismiss();
                            alertDialogList = null;
                        }
                        e.printStackTrace();
                    }
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    // Handle any errors
                    if (alertDialogList != null) {
                        alertDialogList.dismiss();
                        alertDialogList = null;
                    }
                }
            }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                @Override
                public void onComplete(@NonNull Task<Uri> task) {
                    if (alertDialogList != null) {
                        alertDialogList.dismiss();
                        alertDialogList = null;
                    }
                }
            });
        } else {
            atributosList.get(aux).getMediaPlayer().start();
            atributosList.get(aux).setPlay(true);
            adapterAudio.notifyItemChanged(aux);
            adapterAudio.notifyDataSetChanged();
        }
    }


    @Override
    public void onDestroy() {
        if (Login.finalizar) {
            adapterAudio.onDestroy();
        }
        super.onDestroy();

    }


    private void showWait() {
        try {
            alertDialogList = new DialogFragment();

            alertDialogList.setCancelable(false);
            alertDialogList.setStyle(R.layout.dialog_wait, R.style.Theme_AppCompat_Dialog_Alert);

            alertDialogList.show(getFragmentManager(), "dfLista");
        } catch (Exception ist) {

        }
    }


    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        // Make sure that we are currently visible
        if (this.isVisible()) {
            // If we are becoming invisible, then...
            if (!isVisibleToUser) {
                adapterAudio.onDestroy();
                if (alertDialogList != null) {
                    alertDialogList.dismiss();
                    alertDialogList = null;
                }
            }
            pause=true;

        }
    }

    @Override
    public void onPause() {
        if (alertDialogList != null) {
            alertDialogList.dismiss();
            alertDialogList = null;
        }
        pause=true;

        adapterAudio.onDestroy();
        super.onPause();
    }

    @Override
    public void onResume() {
        pause=false;
        super.onResume();
    }
}
