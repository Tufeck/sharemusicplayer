package com.example.sharemusicplayer.fragments.principal.uploadAudio.dialog;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.example.sharemusicplayer.R;

import java.util.ArrayList;
import java.util.List;


public class FragmentExternalAudio extends Fragment {

    private RecyclerView rv;
    private List<AudioAtributos> atributosList;
    private AudioAdapterRecyclerView adapterAudio;
    private LinearLayout layoutSinMusic;
    private Context context;
    private DialogFragment alertDialogExternal;

    public FragmentExternalAudio() {
    }

    @SuppressLint("ValidFragment")
    public FragmentExternalAudio(Context context) {
        this.context = context;

    }

    public class TareaPruebaDatos extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... params) {
            String selection = MediaStore.Audio.Media.IS_MUSIC + " != 0";

            String[] projection = {
                    MediaStore.Audio.Media._ID,
                    MediaStore.Audio.Media.ARTIST,
                    MediaStore.Audio.Media.TITLE,
                    MediaStore.Audio.Media.DATA,
                    MediaStore.Audio.Media.DISPLAY_NAME,
                    MediaStore.Audio.Media.DURATION
            };
            Cursor cursor = null;
            try {
                cursor = getActivity().getContentResolver().query(
                        MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                        projection,
                        selection,
                        null,
                        null);
                while(cursor.moveToNext()){
                    agregarMusic(cursor.getString(2),cursor.getString(3),Integer.parseInt(cursor.getString(5)));
                }
                cursor.close();
            }catch (NullPointerException n){

            }catch (RuntimeException r){

            }

            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            adapterAudio.notifyDataSetChanged();
            verificarSiTenemosMusica();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_select_audio,container,false);

        atributosList = new ArrayList<>();

        rv = (RecyclerView) v.findViewById(R.id.recyclerViewAudio);
        layoutSinMusic = (LinearLayout) v.findViewById(R.id.layoutVacioSinMusica);
        LinearLayoutManager lm = new LinearLayoutManager(getContext());
        rv.setLayoutManager(lm);

        adapterAudio = new AudioAdapterRecyclerView(atributosList,getContext(),(DialogFragmentSelectAudio)getParentFragment());
        rv.setAdapter(adapterAudio);
        if (alertDialogExternal!=null){
            alertDialogExternal.dismiss();
            alertDialogExternal=null;
        }else{
            showWait();
        }

        new TareaPruebaDatos().execute();
        adapterAudio.notifyDataSetChanged();
        verificarSiTenemosMusica();

        return v;
    }

    public void verificarSiTenemosMusica(){
        if(atributosList.isEmpty()){
            layoutSinMusic.setVisibility(View.VISIBLE);
            rv.setVisibility(View.GONE);
        }else{
            layoutSinMusic.setVisibility(View.GONE);
            rv.setVisibility(View.VISIBLE);
        }
        if (alertDialogExternal!=null){
            alertDialogExternal.dismiss();
            alertDialogExternal=null;
        }
    }

    public void agregarMusic(String nombre, String data,int Duracion){
        AudioAtributos postAtributos = new AudioAtributos();
        postAtributos.setNameSong(nombre);
        postAtributos.setData(data);
        postAtributos.setPlay(false);
        postAtributos.setMediaPlayer(MediaPlayer.create(context, Uri.parse(data)));
        int hora = Duracion/3600000;
        int restohora = Duracion%3600000;
        int minuto = restohora/60000;
        int restominuto = restohora%60000;
        int segundo = restominuto/1000 ;
        postAtributos.setDuracion((hora<10 ? "0"+hora : hora) + ":" + (minuto<10 ? "0"+minuto : minuto) + ":" + (segundo<10 ? "0"+segundo : segundo));
        atributosList.add(postAtributos);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        for(int i=0;i<atributosList.size();i++){
            if(atributosList.get(i).getMediaPlayer()!=null){
                atributosList.get(i).getMediaPlayer().stop();
                atributosList.get(i).setMediaPlayer(null);
            }
        }
    }

    private void showWait() {
        try {
            alertDialogExternal = new DialogFragment();

            alertDialogExternal.setCancelable(false);
            alertDialogExternal.setStyle(R.layout.dialog_wait, R.style.Theme_AppCompat_Dialog_Alert);

            alertDialogExternal.show(getFragmentManager(), "dfLista");
        } catch (Exception ist) {

        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        // Make sure that we are currently visible
        if (this.isVisible()) {
            // If we are becoming invisible, then...
            if (!isVisibleToUser) {
                super.onDestroy();
                for(int i=0;i<atributosList.size();i++){
                    if(atributosList.get(i).getMediaPlayer()!=null){
                        atributosList.get(i).getMediaPlayer().pause();
                    }
                    atributosList.get(i).setPlay(false);
                    adapterAudio.notifyDataSetChanged();
                }
                if (alertDialogExternal != null) {
                    alertDialogExternal.dismiss();
                    alertDialogExternal = null;
                }
            }

        }
    }

}
