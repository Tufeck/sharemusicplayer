package com.example.sharemusicplayer.fragments.principal.uploadAudio;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sharemusicplayer.activity.principal.Constants;
import com.example.sharemusicplayer.classes.User;
import com.example.sharemusicplayer.fragments.principal.ComunicadorPrincipal;
import com.example.sharemusicplayer.fragments.principal.uploadAudio.dialog.DialogFragmentSelectAudio;
import com.example.sharemusicplayer.fragments.principal.uploadAudio.dialog.WaitDialog;
import com.example.sharemusicplayer.Preferences;
import com.example.sharemusicplayer.R;
import com.example.sharemusicplayer.classes.Post;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.Date;


public class FragmentUploadAudio extends Fragment implements ComunicadorAudio {
    private int mProgressStatus = 0;
    private Button elegirAudio;
    private TextView audioFileText;
    private TextInputLayout textPost;
    private Button publicar;
    private ComunicadorPrincipal comunicadorPrincipal;
    private FirebaseStorage storage;
    private FirebaseFirestore database;
    private String username;
    private ProgressBar progressBar;
    private DialogFragment alertDialogUpload;
    private boolean pause = false;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_upload_audio,container,false);
        elegirAudio = (Button) v.findViewById(R.id.ElegirArchivo);
        audioFileText = (TextView) v.findViewById(R.id.audioFileText);
        textPost = (TextInputLayout) v.findViewById(R.id.post);
        publicar = (Button) v.findViewById(R.id.publicar);
        username = Preferences.obtenerPreferenceString(getContext(),Preferences.PREFERENCE_USUARIO_LOGIN);
        storage = FirebaseStorage.getInstance();
        database = FirebaseFirestore.getInstance();
        progressBar = v.findViewById(R.id.progressBar);
        comunicadorPrincipal = (ComunicadorPrincipal) getActivity();

        elegirAudio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fragmentManager = getFragmentManager();
                DialogFragmentSelectAudio dialog = new DialogFragmentSelectAudio();
                dialog.show(fragmentManager, "Elige un Audio");
                actualizar();


            }
        });

        publicar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final CollectionReference ref = database.collection("usuarios");
                ref.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                final User user = document.toObject(User.class);
                                String id = user.getIdUser();

                                if (id.equals(username)) {

                                    if (Constants.uri != null) {
                                        CharSequence s = DateFormat.format("dd-MM-yyyy hh:mm:ss", new Date().getTime());
                                        Post p = new Post(
                                                user.getIdUser(),
                                                textPost.getEditText().getText().toString(),
                                                "2", "" + s, Preferences.obtenerPreferenceString(getContext(), Preferences.PREFERENCE_NAME_AUDIO),
                                                Preferences.obtenerPreferenceString(getContext(), Preferences.PREFERENCE_DURACION_TEMP),
                                                Preferences.obtenerPreferenceString(getContext(), Preferences.PREFERENCE_AUDIO_TEMP));

                                        uploadFile(Constants.uri, p, user.getName() + " " + user.getLastname());

                                        textPost.getEditText().setText("");
                                        comunicadorPrincipal.hideKeyboard();
                                        Constants.uri = null;
                                    } else {
                                        Toast toast = Toast.makeText(getContext(), "Elije un audio", Toast.LENGTH_LONG);
                                        toast.show();
                                    }
                                }
                            }

                        }
                    }
                });
            }
        });

        return v;
    }

    public void uploadFile(Uri uri, final Post post, final String nombre){
        final String fileName = System.currentTimeMillis()+"";
        if (alertDialogUpload != null) {
            alertDialogUpload.dismiss();
            alertDialogUpload = null;
        }else{
            showWait();
        }
        StorageReference storageReference = storage.getReference();
        storageReference.child("Uploads").child(fileName).putFile(uri).
                addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                final String url = taskSnapshot.getMetadata().getPath();
                post.setDataMusic(url);
                post.setPostId(fileName);
                CollectionReference cref = database.collection("Posts");
                cref.document(fileName).set(post).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast toast = Toast.makeText(getContext(), "Post subido", Toast.LENGTH_LONG);
                        toast.show();
                        Log.i("Post", "post subido al firebase");

                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        if (alertDialogUpload != null) {
                            alertDialogUpload.dismiss();
                            alertDialogUpload = null;
                        }
                        Log.i("Post", e.getMessage());
                        Toast toast = Toast.makeText(getContext(), "Error al subir post", Toast.LENGTH_LONG);
                        toast.show();
                    }
                });
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                if (alertDialogUpload != null) {
                    alertDialogUpload.dismiss();
                    alertDialogUpload = null;
                }
                Log.i("FailureListener", e.getMessage());
            }
        }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {

                double progress = (100.0 * taskSnapshot.getBytesTransferred() / taskSnapshot.getTotalByteCount());
                progressBar.setProgress((int)progress);
            }
        }).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                progressBar.setProgress(0);
                if (alertDialogUpload != null) {
                    try {
                        alertDialogUpload.dismiss();
                        alertDialogUpload = null;
                    }catch (Exception e){

                    }
                }
                if (!pause) {
                    comunicadorPrincipal.actualizarPost();
                }

            }
        });
    }

    @Override
    public void actualizar() {
        audioFileText.setText(Preferences.obtenerPreferenceString(getContext(),Preferences.PREFERENCE_NAME_AUDIO));

    }

    private void showWait() {
        alertDialogUpload = new DialogFragment();

        alertDialogUpload.setCancelable(false);
        alertDialogUpload.setStyle(R.layout.dialog_wait, R.style.Theme_AppCompat_Dialog_Alert);
        alertDialogUpload.show(getFragmentManager(), "dfupload");
    }


    @Override
    public void onPause() {
        if (alertDialogUpload != null) {
            alertDialogUpload.dismiss();
            alertDialogUpload = null;
        }
        pause=true;
        super.onPause();
    }

    @Override
    public void onResume() {
        pause=false;
        super.onResume();
    }
}
