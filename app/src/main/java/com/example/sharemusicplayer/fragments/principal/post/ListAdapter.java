package com.example.sharemusicplayer.fragments.principal.post;

import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.sharemusicplayer.fragments.principal.uploadAudio.dialog.AudioAtributos;
import com.example.sharemusicplayer.fragments.principal.uploadAudio.dialog.DialogFragmentSelectAudio;
import com.example.sharemusicplayer.fragments.principal.uploadAudio.dialog.WaitDialog;
import com.example.sharemusicplayer.R;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.IOException;
import java.util.List;

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.AudioHolder> {
    private static List<AudioAtributos> atributosList;
    private Context context;
    private DialogFragmentSelectAudio dialogFragmentSelectAudio;
    FirebaseStorage firebaseStorage;
    FirebaseFirestore database;


    public ListAdapter(List<AudioAtributos> atributosList, Context context, DialogFragmentSelectAudio dialogFragmentSelectAudio) {
        this.atributosList = atributosList;
        this.context = context;
        this.dialogFragmentSelectAudio = dialogFragmentSelectAudio;
        firebaseStorage = FirebaseStorage.getInstance();
        database = FirebaseFirestore.getInstance();
        invertirArray();
    }

    @Override
    public AudioHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_list, parent, false);
        return new AudioHolder(v);
    }

    @Override
    public void onBindViewHolder(final AudioHolder holder, final int position) {
        holder.nombre.setText(atributosList.get(position).getNameSong());
        holder.hora.setText(atributosList.get(position).getDuracion());
        holder.nombre.setSelected(true);
        if (holder.play != null) {
            if (!atributosList.get(position).isPlay()) {
                holder.play.setImageResource(R.drawable.ic_action_playback_play);
            } else {
                holder.play.setImageResource(R.drawable.ic_action_playback_pause);
            }
        }
        holder.play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (atributosList.get(position).isPlay()) {
                    atributosList.get(position).getMediaPlayer().pause();
                    atributosList.get(position).setPlay(false);
                    notifyItemChanged(position);
                    notifyDataSetChanged();
                } else {
                    for (int i = 0; i < atributosList.size(); i++) {
                        if (position != i) {
                            if (atributosList.get(i).getMediaPlayer() != null && atributosList.get(i).getMediaPlayer().isPlaying()) {
                                atributosList.get(i).getMediaPlayer().stop();
                                atributosList.get(i).getMediaPlayer().release();
                                atributosList.get(i).setMediaPlayer(null);
                            }
                            atributosList.get(i).setPlay(false);
                            notifyDataSetChanged();
                            notifyItemChanged(i);
                        }
                    }

                    if (atributosList.get(position).getMediaPlayer() == null) {
                        if (WaitDialog.alertDialog == null) {
                            WaitDialog.showWait(context, "Reproduciendo...");
                        } else {
                            WaitDialog.closeWait();
                        }
                        StorageReference storageRef = firebaseStorage.getReference();
                        storageRef.child(atributosList.get(position).getData()).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri uri) {
                                try {
                                    atributosList.get(position).setMediaPlayer(new MediaPlayer());
                                    atributosList.get(position).getMediaPlayer().setDataSource(uri.toString());
                                    atributosList.get(position).getMediaPlayer().setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                                        @Override
                                        public void onPrepared(MediaPlayer mp) {
                                            atributosList.get(position).getMediaPlayer().start();
                                            atributosList.get(position).setPlay(true);

                                            notifyDataSetChanged();
                                            notifyItemChanged(position);
                                            if (WaitDialog.alertDialog != null) {
                                                WaitDialog.closeWait();
                                            }
                                        }
                                    });

                                    atributosList.get(position).getMediaPlayer().prepareAsync();
                                    atributosList.get(position).getMediaPlayer().setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                                        @Override
                                        public void onCompletion(MediaPlayer mp) {
                                            atributosList.get(position).getMediaPlayer().stop();
                                            atributosList.get(position).getMediaPlayer().release();
                                            atributosList.get(position).setMediaPlayer(null);

                                            atributosList.get(position).setPlay(false);
                                            notifyDataSetChanged();
                                            notifyItemChanged(position);
                                        }
                                    });
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception exception) {
                                // Handle any errors
                            }
                        });

                    } else {
                        atributosList.get(position).getMediaPlayer().start();
                        atributosList.get(position).setPlay(true);
                        notifyItemChanged(position);
                        notifyDataSetChanged();
                    }

                }

            }
        });


    }

    @Override
    public int getItemCount() {
        return atributosList.size();
    }

    static class AudioHolder extends RecyclerView.ViewHolder {

        CardView cardView;
        TextView nombre;
        TextView hora;
        ImageButton play;

        public AudioHolder(View itemView) {
            super(itemView);
            cardView = (CardView) itemView.findViewById(R.id.cardViewPostMusic);
            nombre = (TextView) itemView.findViewById(R.id.nameMusic);
            hora = (TextView) itemView.findViewById(R.id.durationMusic);
            play = (ImageButton) itemView.findViewById(R.id.playMusic);
        }
    }

    public void invertirArray() {
        for (int i = 0; i < atributosList.size() / 2; i++) {

            AudioAtributos temp = atributosList.get(i);
            atributosList.set(i, atributosList.get(atributosList.size() - i - 1));
            atributosList.set(atributosList.size() - i - 1, temp);
        }
    }

    public void onDestroy() {
        if (atributosList != null) {
            for (int i = 0; i < atributosList.size(); i++) {
                if (atributosList.get(i).getMediaPlayer() != null) {
                    atributosList.get(i).getMediaPlayer().stop();
                    atributosList.get(i).getMediaPlayer().release();
                    atributosList.get(i).setMediaPlayer(null);
                }
                ListFragment.isPlayList = false;
                atributosList.get(i).setPlay(false);
                notifyDataSetChanged();
                notifyItemChanged(i);
                if (WaitDialog.alertDialog != null) {
                    WaitDialog.closeWait();
                }
            }
        }
    }


}
