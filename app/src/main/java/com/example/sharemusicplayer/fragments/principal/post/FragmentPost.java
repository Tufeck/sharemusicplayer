package com.example.sharemusicplayer.fragments.principal.post;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.sharemusicplayer.activity.principal.MainActivity;
import com.example.sharemusicplayer.classes.User;
import com.example.sharemusicplayer.Preferences;
import com.example.sharemusicplayer.R;
import com.example.sharemusicplayer.classes.Post;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;


import java.util.ArrayList;
import java.util.List;


public class FragmentPost extends Fragment {

    private static RecyclerView rv;
    private static List<PostAtributos> atributosList;
    private static LinearLayout layoutSinSolicitudes;
    private FirebaseFirestore database;
    private DialogFragment alertDialogPost;
    private boolean pause=false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_post, container, false);
        database = FirebaseFirestore.getInstance();
        atributosList = new ArrayList<>();
        final String username = Preferences.obtenerPreferenceString(getContext(), Preferences.PREFERENCE_USUARIO_LOGIN);
        rv = (RecyclerView) v.findViewById(R.id.amigosRecyclerView);
        layoutSinSolicitudes = (LinearLayout) v.findViewById(R.id.layoutVacioSolicitudes);
        final LinearLayoutManager linearLayoutManager =
                new LinearLayoutManager(getActivity());
        linearLayoutManager.setStackFromEnd(true);
        linearLayoutManager.setReverseLayout(true);
        rv.setLayoutManager(linearLayoutManager);

        MainActivity.postAdapter = new PostAdapter(atributosList, getContext(), false);
        rv.setAdapter(MainActivity.postAdapter);
        if (alertDialogPost != null) {
            alertDialogPost.dismiss();
            alertDialogPost = null;
        } else{
            showWait();
        }
        final CollectionReference ref = database.collection("Posts");
        ref.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    for (final QueryDocumentSnapshot postDocument : task.getResult()) {

                        final Post p = postDocument.toObject(Post.class);
                        if (p.getIdUser().equals(username)) {


                            final CollectionReference ref = database.collection("usuarios");
                            ref.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                @Override
                                public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                    if (task.isSuccessful()) {
                                        for (QueryDocumentSnapshot document : task.getResult()) {

                                            User user = document.toObject(User.class);
                                            if (user.getIdUser().equals(username)) {
                                                agregarPost(p, getContext(), user.getName() + " " + user.getLastname());

                                            }
                                        }
                                    } else {
                                        if (alertDialogPost != null) {
                                            alertDialogPost.dismiss();
                                            alertDialogPost = null;
                                        }
                                        Log.i("Task fail", "no se han podido cargar los usuarios");
                                    }
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Log.i("Task fail", e.getMessage());
                                    if (alertDialogPost != null) {
                                        alertDialogPost.dismiss();
                                        alertDialogPost = null;
                                    }
                                    verificarSiTenemosPost();

                                }
                            }).addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                                @Override
                                public void onComplete(@NonNull Task<QuerySnapshot> task) {
                                    if (alertDialogPost != null) {
                                        alertDialogPost.dismiss();
                                        alertDialogPost = null;
                                    }
                                    verificarSiTenemosPost();
                                    linearLayoutManager.scrollToPosition(atributosList.size());

                                }
                            });
                        }

                    }
                } else {
                    Log.i("Task fail", "no se han podido cargar los posts");
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.i("Task fail", e.getMessage());
                if (alertDialogPost != null) {
                    alertDialogPost.dismiss();
                    alertDialogPost = null;
                }
                verificarSiTenemosPost();
            }

        }).addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (alertDialogPost != null) {
                    alertDialogPost.dismiss();
                    alertDialogPost = null;
                }
                verificarSiTenemosPost();
            }
        });


        return v;
    }

    public static void verificarSiTenemosPost() {
        if (atributosList.isEmpty()) {
            layoutSinSolicitudes.setVisibility(View.VISIBLE);
            rv.setVisibility(View.GONE);

        } else {
            layoutSinSolicitudes.setVisibility(View.GONE);
            rv.setVisibility(View.VISIBLE);
        }
    }

    public static void agregarPost(Post post, Context c, String nombre) {

        PostAtributos postAtributos = new PostAtributos();
        postAtributos.setId(post.getIdUser());
        postAtributos.setFotoDePerfil(R.drawable.ic_action_user);
        postAtributos.setPost(post.getPost());
        postAtributos.setHora(post.getDate());
        postAtributos.setType(post.getType());
        String postid = post.getPostId();
        postAtributos.setIdGenerate(post.getPostId());
        postAtributos.setNombre(nombre);
        postAtributos.setPlay(false);
        postAtributos.setMediaPlayer(null);
        postAtributos.setDataMusic(post.getDataMusic());
        postAtributos.setNombreMusic(post.getNombreMusic());
        postAtributos.setHoraMusic(post.getMusciDuration());
        if (atributosList.isEmpty()) {
            atributosList.add(postAtributos);
        } else {
            atributosList.add(atributosList.get(atributosList.size() - 1));
            atributosList.set(atributosList.size() - 1, postAtributos);
        }


        verificarSiTenemosPost();
        MainActivity.postAdapter.notifyDataSetChanged();
    }
    private void showWait() {
        alertDialogPost = new DialogFragment();

        alertDialogPost.setCancelable(false);
        alertDialogPost.setStyle(R.layout.dialog_wait, R.style.Theme_AppCompat_Dialog_Alert);
        alertDialogPost.show(getFragmentManager(), "dfPost");
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        // Make sure that we are currently visible
        if (this.isVisible()) {
            // If we are becoming invisible, then...
            if (!isVisibleToUser) {
                MainActivity.postAdapter.onDestroy();

            }

        }
    }

    @Override
    public void onPause() {
        if (alertDialogPost != null) {
            alertDialogPost.dismiss();
            alertDialogPost = null;
        }
        pause=true;

        MainActivity.postAdapter.onDestroy();
        super.onPause();
    }

    @Override
    public void onResume() {
        pause=false;
        super.onResume();
    }
}
