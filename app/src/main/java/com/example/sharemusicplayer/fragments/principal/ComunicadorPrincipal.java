package com.example.sharemusicplayer.fragments.principal;


public interface ComunicadorPrincipal {
    void actualizarPost();
    void finalizar();
    void hideKeyboard();
    void actualizarFragmentAudio();
}
