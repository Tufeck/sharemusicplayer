package com.example.sharemusicplayer.fragments.principal.post;

import android.content.Context;
import android.content.DialogInterface;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sharemusicplayer.Preferences;
import com.example.sharemusicplayer.R;
import com.example.sharemusicplayer.fragments.principal.uploadAudio.dialog.WaitDialog;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.IOException;
import java.util.List;

public class PostAdapter extends RecyclerView.Adapter<PostAdapter.PostHolder> {
    private static List<PostAtributos> atributosList;
    private Context context;

    private FirebaseStorage firebaseStorage;
    private FirebaseFirestore database;
    private boolean search;


    public PostAdapter(List<PostAtributos> atributosList, Context context, boolean search) {
        this.atributosList = atributosList;
        this.context = context;
        firebaseStorage = FirebaseStorage.getInstance();
        database = FirebaseFirestore.getInstance();
        this.search = search;
    }

    @Override
    public int getItemViewType(int position) {
        return Integer.parseInt(atributosList.get(position).getType());
    }

    @Override
    public PostHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case 1:
                return new PostHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_posts, parent, false), 1);
            case 2:
                return new PostHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_posts_type_2, parent, false), 2);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(final PostHolder holder, final int position) {
        holder.nombre.setText(atributosList.get(position).getNombre());
        holder.post.setText(atributosList.get(position).getPost());
        holder.hora.setText(atributosList.get(position).getHora());
        if (holder.play !=null) {
            if (!atributosList.get(position).isPlay()) {
                holder.play.setImageResource(R.drawable.ic_action_playback_play);
            } else {
                holder.play.setImageResource(R.drawable.ic_action_playback_pause);
            }
        }
        if (atributosList.get(position).getType().equals("2")) {
            holder.nombreMusic.setText(atributosList.get(position).getNombreMusic());
            holder.horaMusic.setText(atributosList.get(position).getHoraMusic());
            holder.play.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (atributosList.get(position).isPlay()) {
                        atributosList.get(position).getMediaPlayer().pause();
                        atributosList.get(position).setPlay(false);
                        notifyItemChanged(position);
                    }else {
                        for (int i = 0; i < atributosList.size(); i++) {
                            if (position!=i) {
                                if (atributosList.get(i).getMediaPlayer()!=null) {
                                    atributosList.get(i).getMediaPlayer().stop();
                                    atributosList.get(i).getMediaPlayer().release();
                                    atributosList.get(i).setMediaPlayer(null);
                                }
                                atributosList.get(i).setPlay(false);
                                notifyItemChanged(i);
                            }
                        }
                        if (atributosList.get(position).getMediaPlayer()==null) {
                            if (WaitDialog.alertDialog==null) {
                                WaitDialog.showWait(context, "Reproduciendo...");
                            }else{
                                WaitDialog.closeWait();
                            }
                            StorageReference storageRef = firebaseStorage.getReference();
                            storageRef.child(atributosList.get(position).getDataMusic()).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                    try {
                                        atributosList.get(position).setMediaPlayer(new MediaPlayer());
                                        atributosList.get(position).getMediaPlayer().setDataSource(uri.toString());
                                        atributosList.get(position).getMediaPlayer().setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                                            @Override
                                            public void onPrepared(MediaPlayer mp) {
                                                if (WaitDialog.alertDialog!=null){
                                                    WaitDialog.closeWait();
                                                }
                                                atributosList.get(position).getMediaPlayer().start();
                                                atributosList.get(position).setPlay(true);
                                                notifyItemChanged(position);
                                            }
                                        });
                                        atributosList.get(position).getMediaPlayer().prepareAsync();
                                        atributosList.get(position).getMediaPlayer().setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                                            @Override
                                            public void onCompletion(MediaPlayer mp) {
                                                atributosList.get(position).getMediaPlayer().stop();
                                                atributosList.get(position).getMediaPlayer().release();
                                                atributosList.get(position).setMediaPlayer(null);

                                                atributosList.get(position).setPlay(false);
                                                notifyDataSetChanged();
                                                notifyItemChanged(position);
                                            }
                                        });
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception exception) {
                                    // Handle any errors
                                }
                            });
                        }else{
                            atributosList.get(position).getMediaPlayer().start();
                            atributosList.get(position).setPlay(true);
                            notifyItemChanged(position);
                            notifyDataSetChanged();
                        }

                    }
                }
            });
        }

        holder.cardView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (!search) {
                    if (atributosList.get(position).getId().equalsIgnoreCase(Preferences.obtenerPreferenceString(context, Preferences.PREFERENCE_USUARIO_LOGIN))) {
                        new AlertDialog.Builder(v.getContext())
                                .setMessage("¿Estas seguro de eliminar esta publicación?")
                                .setPositiveButton("OK",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                CollectionReference ref = database.collection("Posts");
                                                ref.document(atributosList.get(position).getIdGenerate()).delete();
                                                atributosList.remove(position);
                                                Toast toast = Toast.makeText(context, "Post eliminado", Toast.LENGTH_LONG);
                                                toast.show();
                                                notifyDataSetChanged();
                                            }
                                        })
                                .setNegativeButton("Cancelar",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.cancel();
                                            }
                                        }).show();
                    }
                }
                return true;
            }
        });

    }

    @Override
    public int getItemCount() {
        return atributosList.size();
    }




    static class PostHolder extends RecyclerView.ViewHolder {

        CardView cardView;
        TextView nombre;
        TextView post;
        TextView hora;

        TextView nombreMusic;
        TextView horaMusic;
        ImageButton play;

        public PostHolder(View itemView, int type) {
            super(itemView);
            nombre = itemView.findViewById(R.id.nombreUsuarioAmigo);
            post = itemView.findViewById(R.id.mensajeAmigos);
            switch (type) {
                case 1:
                    cardView = itemView.findViewById(R.id.cardViewPostMusic);

                    hora = itemView.findViewById(R.id.horaAmigos);
                    break;
                case 2:
                    cardView = itemView.findViewById(R.id.cardViewPostMusic);
                    hora = itemView.findViewById(R.id.horaAmigos);
                    nombreMusic = itemView.findViewById(R.id.nameMusic);
                    horaMusic = itemView.findViewById(R.id.durationMusic);
                    play = itemView.findViewById(R.id.playMusic);
                    break;
            }
        }
    }

    public void onDestroy() {
        if (atributosList!=null) {
            for (int i = 0; i < atributosList.size(); i++) {
                if (atributosList.get(i).getMediaPlayer() != null) {
                    atributosList.get(i).getMediaPlayer().stop();
                    atributosList.get(i).getMediaPlayer().release();
                    atributosList.get(i).setMediaPlayer(null);
                }
                ListFragment.isPlayList = false;
                atributosList.get(i).setPlay(false);
                notifyDataSetChanged();
                notifyItemChanged(i);
                if (WaitDialog.alertDialog!=null){
                    WaitDialog.closeWait();
                }
            }
        }
    }

}
