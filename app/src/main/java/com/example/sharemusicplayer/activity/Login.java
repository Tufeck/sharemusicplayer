package com.example.sharemusicplayer.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.example.sharemusicplayer.activity.principal.MainActivity;
import com.example.sharemusicplayer.activity.principal.Constants;
import com.example.sharemusicplayer.classes.User;
import com.example.sharemusicplayer.Preferences;
import com.example.sharemusicplayer.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.regex.Pattern;

public class Login extends AppCompatActivity implements View.OnClickListener {

    private EditText user, password;
    private Button login;
    public static boolean finalizar = false;
    private RadioButton rb_sesion;

    private boolean isActivateRadioButton;
    private FirebaseFirestore database;
    private DialogFragment alertDialogLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);

        if (connected()) {
            if (Preferences.obtenerPreferenceBoolean(this, Preferences.PREFERENCE_ESTADO_BUTTON_SESION)) {

                iniciarActividadSiguiente();
                if (finalizar) {
                    finish();
                }

            }
        }
        database = FirebaseFirestore.getInstance();


        user = findViewById(R.id.Login_User_campo);
        password = findViewById(R.id.Login_Password_campo);
        login = (Button) findViewById(R.id.login_ingresar);
        login.setOnClickListener(this);

        rb_sesion = (RadioButton) findViewById(R.id.Login_Sesion);

        isActivateRadioButton = rb_sesion.isChecked(); //DESACTIVADO

        rb_sesion.setOnClickListener(new View.OnClickListener() {
            //ACTIVADO
            @Override
            public void onClick(View v) {
                if (isActivateRadioButton) {
                    rb_sesion.setChecked(false);
                }
                isActivateRadioButton = rb_sesion.isChecked();
            }
        });

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (shouldShowRequestPermissionRationale(Manifest.permission.READ_EXTERNAL_STORAGE)) {
                }
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
            }
        }

    }

    private void iniciarActividadSiguiente() {
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    @Override
    public void onClick(View v) {

        if (connected()) {

            hideKeyboard();
            final String username = this.user.getText().toString();
            final String pass = password.getText().toString();

            boolean a = ValidarUser(username);
            boolean b = ValidarPassWord(pass);

            if (a && b) {
                final CollectionReference ref = database.collection("usuarios");
                final boolean[] result = new boolean[1];
                result[0] = false;
                if (alertDialogLogin==null){
                    showWait();
                }else{
                    alertDialogLogin.dismiss();
                    alertDialogLogin=null;
                }
                ref.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            boolean exists = false;
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                if (alertDialogLogin!=null){
                                    alertDialogLogin.dismiss();
                                    alertDialogLogin=null;
                                }
                                User user = document.toObject(User.class);
                                String id = user.getIdUser();

                                if (id.equals(username)) {
                                    exists = true;
                                    if (user.getPassword().equals(pass)) {
                                        Constants.userName = id;

                                        finalizar = rb_sesion.isChecked();
                                        Preferences.savePreferenceBoolean(Login.this, rb_sesion.isChecked(), Preferences.PREFERENCE_ESTADO_BUTTON_SESION);
                                        Preferences.savePreferenceString(Login.this, id, Preferences.PREFERENCE_USUARIO_LOGIN);
                                        iniciarActividadSiguiente();
                                    } else {
                                        Toast.makeText(Login.this, "Contraseña incorrecta", Toast.LENGTH_SHORT).show();
                                    }
                                    break;
                                }
                            }
                            if (!exists) {
                                Toast.makeText(Login.this, "El usuario no existe", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(Login.this, "Error al abrir base de datos", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }else{
            Toast.makeText(Login.this, "Sin Internet", Toast.LENGTH_SHORT).show();
        }
    }

    private void hideKeyboard() {
        View view = getCurrentFocus();
        if (view != null) {
            ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).
                    hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    private boolean ValidarUser(String Usuario) {
        Pattern patron = Pattern.compile("^[a-zA-Z0-9]+$");
        if (Usuario.length() < 1) {
            user.setError("User Vacio");
            return false;
        }
        if (!patron.matcher(Usuario).matches() || Usuario.length() > 20) {
            user.setError("User Invalido");
            return false;
        } else {
            user.setError(null);
        }
        return true;
    }

    private boolean ValidarPassWord(String PassWord) {
        if (PassWord.length() < 1) {
            password.setError("Contraseña Vacia");
            return false;
        } else {
            password.setError(null);
        }
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        switch (id) {
            case R.id.crearUsuario:
                startActivity(new Intent(this, Register.class));
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public boolean connected(){
        boolean connected = false;
        ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        if(connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            //we are connected to a network
            connected = true;
        }
        else
            connected = false;
        return connected;
    }

    private void showWait() {
        try {
            alertDialogLogin = new DialogFragment();

            alertDialogLogin.setCancelable(false);
            alertDialogLogin.setStyle(R.layout.dialog_wait, R.style.Theme_AppCompat_Dialog_Alert);

            alertDialogLogin.show(getSupportFragmentManager(), "dfLogin");
        } catch (Exception ist) {

        }
    }
}
