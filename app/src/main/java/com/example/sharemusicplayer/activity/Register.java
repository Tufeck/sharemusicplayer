package com.example.sharemusicplayer.activity;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.sharemusicplayer.classes.User;
import com.example.sharemusicplayer.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Locale;

public class Register extends AppCompatActivity {


    private Spinner spCountry;
    private Button button;

    private TextInputLayout user, nombre, apellidos, song, password, passwordRepeat, instrumento;
    private RadioButton rdHombre;
    private RadioButton rdMujer;
    private FirebaseStorage storage;
    private FirebaseFirestore database;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        storage = FirebaseStorage.getInstance();
        database = FirebaseFirestore.getInstance();
        Locale[] locale = Locale.getAvailableLocales();
        ArrayList<String> countries = new ArrayList<String>();
        String country;
        for (Locale loc : locale) {
            country = loc.getDisplayCountry();
            if (country.length() > 0 && !countries.contains(country)) {
                countries.add(country);
            }
        }
        Collections.sort(countries, String.CASE_INSENSITIVE_ORDER);

        user = (TextInputLayout) findViewById(R.id.userRegister);
        nombre = (TextInputLayout) findViewById(R.id.nombreRegistro);
        apellidos = (TextInputLayout) findViewById(R.id.apellidosRegistro);
        song = (TextInputLayout) findViewById(R.id.preguntaSecreta);
        password = (TextInputLayout) findViewById(R.id.password);
        passwordRepeat = (TextInputLayout) findViewById(R.id.repetirPassword);
        instrumento = (TextInputLayout) findViewById(R.id.preguntaOpcional);

        rdHombre = (RadioButton) findViewById(R.id.RDhombre);
        rdMujer = (RadioButton) findViewById(R.id.RDmujer);

        rdHombre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rdMujer.setChecked(false);
            }
        });

        rdMujer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rdHombre.setChecked(false);
            }
        });

        spCountry = (Spinner) findViewById(R.id.spinnerPaises);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, countries);
        spCountry.setAdapter(adapter);
        spCountry.setSelection(adapter.getPosition("Spain"));

        button = (Button) findViewById(R.id.buttonRegistro);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                verificarExistenciaUsuario();
            }
        });

    }

    public void verificarRegistro(String user, final String nombre, final String apellido, final String music, final String password, final String instrument, final String country, final String sex) {
        if (!user.isEmpty() && !nombre.isEmpty() && !apellido.isEmpty() && !music.isEmpty() && !country.isEmpty() && !sex.isEmpty()) {
            User usuarioClass = new User();
            usuarioClass.setIdUser(user);
            usuarioClass.setName(nombre);
            usuarioClass.setLastname(apellido);
            usuarioClass.setMusic(music);
            if (instrument.isEmpty()) {
                usuarioClass.setInstrument("No tienes ningún instrumento");
            }else{
                usuarioClass.setInstrument(instrument);
            }
            usuarioClass.setCountry(country);
            usuarioClass.setSex(sex);
            usuarioClass.setPassword(password);
            CollectionReference cref = database.collection("usuarios");
            cref.document(user).set(usuarioClass).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    Log.i("Succes register", "Se ha añadido correctamente");
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(Register.this, "Fallo al registrarse", Toast.LENGTH_SHORT).show();
                    finish();
                    Log.i("FailureListener", e.getMessage());
                }
            });

        }
    }

    private boolean validarTexto(TextInputLayout e, String s) {
        if ((s.trim()).isEmpty()) {
            e.setError("campo invalido");
            return false;
        } else {
            e.setError(null);
            return true;
        }
    }

    private boolean validarPassword(TextInputLayout pR, String s, String sR) {
        if (!s.equals(sR)) {
            pR.setError("Las contraseñas deben ser iguales");
            return false;
        } else {
            pR.setError(null);
            return true;
        }
    }

    private void verificarExistenciaUsuario() {
        final CollectionReference ref = database.collection("usuarios");
        final boolean[] result = new boolean[1];
        result[0] = false;
        ref.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    String userText = user.getEditText().getText().toString();
                    String nombreText = nombre.getEditText().getText().toString();
                    String apellidoText = apellidos.getEditText().getText().toString();
                    String songText = song.getEditText().getText().toString();
                    String passwordText = password.getEditText().getText().toString();
                    String passwordRepeatText = passwordRepeat.getEditText().getText().toString();
                    String instrumentoText = instrumento.getEditText().getText().toString();
                    boolean va = validarTexto(user, userText)
                            && validarTexto(nombre, nombreText)
                            && validarTexto(apellidos, apellidoText)
                            && validarTexto(song, songText)
                            && validarTexto(password, passwordText)
                            && validarTexto(passwordRepeat, passwordRepeatText);
                    if (va) {
                        boolean exists = false;
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            User user = document.toObject(User.class);
                            String id = user.getIdUser();

                            if (id.equals(userText)) {
                                exists = true;
                                break;
                            }
                        }
                        if (exists) {
                            Toast.makeText(Register.this, "El usuario ya existe", Toast.LENGTH_SHORT).show();
                        } else {
                            if (va) {
                                if (validarPassword(passwordRepeat, passwordText, passwordRepeatText)) {
                                    verificarRegistro(
                                            userText,
                                            nombreText,
                                            apellidoText,
                                            songText,
                                            passwordText,
                                            instrumentoText,
                                            spCountry.getSelectedItem().toString().trim(),
                                            rdHombre.isChecked() ? "hombre" : "mujer".trim());
                                }
                            }
                        }
                    }
                } else {
                    Log.d("Error", "Error getting documents: ", task.getException());
                }
            }
        });
    }

}
