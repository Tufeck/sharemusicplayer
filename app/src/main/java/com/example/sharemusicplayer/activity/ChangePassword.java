package com.example.sharemusicplayer.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.Toast;

import com.example.sharemusicplayer.activity.principal.MainActivity;
import com.example.sharemusicplayer.classes.User;
import com.example.sharemusicplayer.Preferences;
import com.example.sharemusicplayer.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.HashMap;
import java.util.Map;


public class ChangePassword extends AppCompatActivity {

    private TextInputLayout song;
    private TextInputLayout password, passwordRepeat;
    private Button cambiar;
    private FirebaseFirestore database;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        database = FirebaseFirestore.getInstance();
        final String username = Preferences.obtenerPreferenceString(this, Preferences.PREFERENCE_USUARIO_LOGIN);
        song = (TextInputLayout) findViewById(R.id.songRecuperar);
        cambiar = (Button) findViewById(R.id.buttonCambiar);
        password = (TextInputLayout) findViewById(R.id.password);
        passwordRepeat = (TextInputLayout) findViewById(R.id.repetirPassword);

        cambiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();

                final String music = song.getEditText().getText().toString();
                boolean b = validarTexto(song,music);
                final CollectionReference ref = database.collection("usuarios");
                ref.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                final User user = document.toObject(User.class);
                                String id = user.getIdUser();

                                if (id.equals(username)) {
                                    if (music.equals(user.getMusic())){
                                        String passwordText = password.getEditText().getText().toString();
                                        String passwordRepeatText = passwordRepeat.getEditText().getText().toString();

                                        boolean va = validarTexto(password,passwordText) && validarTexto(passwordRepeat,passwordRepeatText);
                                        if(va){
                                            if(validarPassword(passwordRepeat,passwordText,passwordRepeatText)){
                                                Map<String, Object> data = new HashMap<>();
                                                data.put("password", passwordText);
                                                ref.document(username).update(data);
                                                Toast.makeText(ChangePassword.this, "La contraseña se cambio correctamente", Toast.LENGTH_SHORT).show();
                                                finish();
                                            }
                                        }
                                    }else{
                                        Toast toast = Toast.makeText(getApplicationContext(), "Canción erronea", Toast.LENGTH_LONG);
                                        toast.show();
                                        startActivity(new Intent(getApplicationContext(), MainActivity.class));
                                    }
                                }
                            }

                        }
                    }
                });

            }
        });

    }

    public void hideKeyboard() {
        View view = getCurrentFocus();
        if (view != null) {
            ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).
                    hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    private boolean validarTexto(TextInputLayout e,String s){
        if ((s.trim()).isEmpty()) {
            e.setError("campo invalido");
            return false;
        } else{
            e.setError(null);
            return true;
        }
    }

    private boolean validarPassword(TextInputLayout pR,String s,String sR){
        if (!s.equals(sR)) {
            pR.setError("Las contraseñas deben ser iguales");
            return false;
        } else{
            pR.setError(null);
            return true;
        }
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(getApplicationContext(), MainActivity.class));
        super.onBackPressed();
    }
}
