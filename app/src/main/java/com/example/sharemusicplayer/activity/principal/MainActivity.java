package com.example.sharemusicplayer.activity.principal;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.example.sharemusicplayer.activity.ChangePassword;
import com.example.sharemusicplayer.activity.EditProfile;
import com.example.sharemusicplayer.activity.Login;
import com.example.sharemusicplayer.activity.SearchActivity;
import com.example.sharemusicplayer.fragments.principal.ComunicadorPrincipal;
import com.example.sharemusicplayer.fragments.principal.post.ListAdapter;
import com.example.sharemusicplayer.fragments.principal.post.PostAdapter;
import com.example.sharemusicplayer.fragments.principal.uploadAudio.FragmentUploadAudio;
import com.example.sharemusicplayer.Preferences;
import com.example.sharemusicplayer.R;


public class MainActivity extends AppCompatActivity implements ComunicadorPrincipal {

    private TabLayout tabLayout;
    private ViewPager viewPager;
    public static PostAdapter postAdapter;
    private TextView textView;
    private MainActivity mainActivity = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);
        tabLayout = (TabLayout) findViewById(R.id.tabLayoutUsuarios);
        viewPager = (ViewPager) findViewById(R.id.viewPagerUsuarios);
        tabLayout.setupWithViewPager(viewPager);
        viewPager.setAdapter(new MainAdapter(getSupportFragmentManager(), this));
        textView = (TextView) findViewById(R.id.usuarioName);
        textView.setText(Preferences.obtenerPreferenceString(this,Preferences.PREFERENCE_USUARIO_LOGIN));
    }

    @Override
    public void actualizarPost() {
        viewPager.setCurrentItem(0);
    }

    @Override
    public void finalizar() {
        finish();
    }

    @Override
    public void hideKeyboard() {
        View view = getCurrentFocus();
        if (view != null) {
            ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).
                    hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    @Override
    public void actualizarFragmentAudio() {
        Fragment page = getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.viewPagerUsuarios + ":" + viewPager.getCurrentItem());
        ((FragmentUploadAudio)page).actualizar();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_principal,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        switch (id){
            case R.id.editarPerfil:
                Intent intent = new Intent(this, EditProfile.class);
                startActivity(intent);
                finish();
                break;
            case R.id.cambiarPassword:
                startActivity(new Intent(this, ChangePassword.class));
                finish();
                break;
            case R.id.searchUser:
                startActivity(new Intent(this, SearchActivity.class));
                finish();
                break;
            case R.id.cerrarSesion:
                Login.finalizar=false;
                Preferences.savePreferenceBoolean(this,false,Preferences.PREFERENCE_ESTADO_BUTTON_SESION);
                startActivity(new Intent(this, Login.class));
                finish();
                break;
            case R.id.ayuda:
                Uri uri = Uri.parse("https://drive.google.com/file/d/1W5aHGIaAGGgM7gYFt8ZGizXbqawqRDnK/view?usp=sharing");
                Intent web = new Intent(Intent.ACTION_VIEW);
                web.setData(uri);
                startActivity(web);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Login.finalizar=true;
        super.onBackPressed();

    }

}
