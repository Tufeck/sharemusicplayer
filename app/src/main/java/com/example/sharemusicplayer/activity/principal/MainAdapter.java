package com.example.sharemusicplayer.activity.principal;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.sharemusicplayer.fragments.chat.FragmentChat;
import com.example.sharemusicplayer.fragments.principal.perfil.FragmentProfile;
import com.example.sharemusicplayer.fragments.principal.post.FragmentPost;
import com.example.sharemusicplayer.fragments.principal.post.ListAdapter;
import com.example.sharemusicplayer.fragments.principal.post.ListFragment;
import com.example.sharemusicplayer.fragments.principal.post.PostAdapter;
import com.example.sharemusicplayer.fragments.principal.uploadAudio.FragmentUploadAudio;


public class MainAdapter extends FragmentPagerAdapter {

    Context context;
    public MainAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.context = context;
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            return new FragmentPost();
        } else if (position == 1) {
            return new FragmentProfile();
        } else if (position == 2) {
            return new ListFragment();
        } else if (position == 3) {
            return new FragmentUploadAudio();
        } else if (position == 4) {
            return new FragmentChat();
        } else return null;
    }

    @Override
    public int getCount() {
        return 5;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (position == 0) return "Home";
        else if (position == 1) return "Publicar";
        else if (position == 2) return "Lista de reproducción";
        else if (position == 3) return "Subir Canción";
        else if (position == 4) return "Chat";
        return null;
    }



}
