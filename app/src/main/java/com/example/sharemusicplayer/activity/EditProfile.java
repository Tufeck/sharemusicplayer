package com.example.sharemusicplayer.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sharemusicplayer.activity.principal.MainActivity;
import com.example.sharemusicplayer.classes.User;
import com.example.sharemusicplayer.fragments.principal.uploadAudio.dialog.WaitDialog;
import com.example.sharemusicplayer.Preferences;
import com.example.sharemusicplayer.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;


public class EditProfile extends AppCompatActivity {

    private Spinner spCountry;
    private Button aceptar, cancelar;

    private User user;
    private TextInputLayout nombre, apellidos, song, instrumento;
    private RadioButton rdHombre;
    private RadioButton rdMujer;
    private FirebaseFirestore database;
    private String username;
    TextView textView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editar_perfil);
        textView= findViewById(R.id.usuarioName);
        database = FirebaseFirestore.getInstance();
        username = Preferences.obtenerPreferenceString(this, Preferences.PREFERENCE_USUARIO_LOGIN);
        textView.setText(username);
        Locale[] locale = Locale.getAvailableLocales();
        ArrayList<String> countries = new ArrayList<String>();
        String country;
        for (Locale loc : locale) {
            country = loc.getDisplayCountry();
            if (country.length() > 0 && !countries.contains(country)) {
                countries.add(country);
            }
        }
        Collections.sort(countries, String.CASE_INSENSITIVE_ORDER);

        nombre = (TextInputLayout) findViewById(R.id.nombreRegistro);
        apellidos = (TextInputLayout) findViewById(R.id.apellidosRegistro);
        song = (TextInputLayout) findViewById(R.id.preguntaSecreta);
        instrumento = (TextInputLayout) findViewById(R.id.preguntaOpcional);

        rdHombre = (RadioButton) findViewById(R.id.RDhombre);
        rdMujer = (RadioButton) findViewById(R.id.RDmujer);

        rdHombre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rdMujer.setChecked(false);
            }
        });

        rdMujer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rdHombre.setChecked(false);
            }
        });
        spCountry = (Spinner) findViewById(R.id.spinnerPaises);
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, countries);
        spCountry.setAdapter(adapter);
        WaitDialog.showWait(EditProfile.this, "Cargando datos...");
        final CollectionReference ref = database.collection("usuarios");
        final boolean[] result = new boolean[1];
        result[0] = false;
        ref.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    for (QueryDocumentSnapshot document : task.getResult()) {
                        User user = document.toObject(User.class);
                        String id = user.getIdUser();

                        if (id.equals(username)) {
                            spCountry.setSelection(adapter.getPosition(user.getCountry()));
                            rdHombre.setChecked(user.getSex().equalsIgnoreCase("hombre"));
                            rdMujer.setChecked(!user.getSex().equalsIgnoreCase("hombre"));

                            nombre.getEditText().setText(user.getName());
                            apellidos.getEditText().setText(user.getLastname());
                            song.getEditText().setText(user.getMusic());
                            instrumento.getEditText().setText(user.getInstrument().isEmpty() ? "No tienes ningun instrumento" : user.getInstrument());
                            break;
                        }
                        if (WaitDialog.alertDialog!=null){
                            WaitDialog.closeWait();
                        }
                    }
                }
            }
        });



        aceptar = (Button) findViewById(R.id.buttonAceptar);
        cancelar = (Button) findViewById(R.id.buttonCancelar);

        cancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), MainActivity.class));
                finish();
            }
        });

        aceptar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nombreText = nombre.getEditText().getText().toString();
                String apellidoText = apellidos.getEditText().getText().toString();
                String songText = song.getEditText().getText().toString();
                String instrumentoText = instrumento.getEditText().getText().toString();
                boolean va = validarTexto(nombre, nombreText)
                        && validarTexto(apellidos, apellidoText)
                        && validarTexto(song, songText);
                if (va) {
                    verificarRegistro(
                            nombreText,
                            apellidoText,
                            songText,
                            instrumentoText,
                            spCountry.getSelectedItem().toString().trim(),
                            rdHombre.isChecked() ? "hombre" : "mujer".trim());
                }

            }
        });

    }

    public void verificarRegistro(final String nombre, final String apellido, final String music, final String instrument, final String country, final String sex) {
        if (!nombre.isEmpty() && !apellido.isEmpty() && !music.isEmpty() && !country.isEmpty() && !sex.isEmpty()) {

            final CollectionReference ref = database.collection("usuarios");
            ref.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                @Override
                public void onComplete(@NonNull Task<QuerySnapshot> task) {
                    if (task.isSuccessful()) {
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            final User user = document.toObject(User.class);
                            String id = user.getIdUser();

                            if (id.equals(username)) {
                                Map<String, Object> data = new HashMap<>();
                                data.put("name", nombre);
                                data.put("lastname", apellido);
                                data.put("music", music);
                                if (instrument.isEmpty()) {
                                    data.put("instrument", "No tienes ningún instrumento");
                                }else{
                                    data.put("instrument", instrument);
                                }
                                data.put("country", country);
                                data.put("sex", sex);
                                ref.document(username).update(data).addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        Toast.makeText(EditProfile.this, "se actualizaron los datos correctamente", Toast.LENGTH_SHORT).show();
                                    }
                                }).addOnFailureListener(new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        Toast.makeText(EditProfile.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                                    }
                                });

                                finish();
                            }

                        }
                    }

                }

            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(EditProfile.this, "Nno se han podido actualizar los datos", Toast.LENGTH_SHORT).show();
                }
            });
            startActivity(new Intent(getApplicationContext(), MainActivity.class));
            finish();
        }
    }

    private boolean validarTexto(TextInputLayout e, String s) {
        if ((s.trim()).isEmpty()) {
            e.setError("campo invalido");
            return false;
        } else {
            e.setError(null);
            return true;
        }
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(getApplicationContext(), MainActivity.class));
        super.onBackPressed();
    }
}
