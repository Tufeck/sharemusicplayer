package com.example.sharemusicplayer.activity;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.sharemusicplayer.fragments.principal.post.FragmentPostSearch;
import com.example.sharemusicplayer.fragments.principal.post.ListFragmentSearch;


public class SearchAdapter extends FragmentPagerAdapter {


    public SearchAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        if(position==0) return new FragmentPostSearch();
        else if(position==1)return new ListFragmentSearch();
        else return null;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if(position==0) return "Home";
        else if(position==1)return "Lista de reproducciones";
        return null;
    }
}
