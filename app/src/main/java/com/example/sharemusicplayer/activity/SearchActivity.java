package com.example.sharemusicplayer.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.sharemusicplayer.activity.principal.Constants;
import com.example.sharemusicplayer.activity.principal.MainActivity;
import com.example.sharemusicplayer.classes.User;
import com.example.sharemusicplayer.fragments.principal.post.ListAdapter;
import com.example.sharemusicplayer.fragments.principal.uploadAudio.dialog.WaitDialog;
import com.example.sharemusicplayer.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

public class SearchActivity extends AppCompatActivity{
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private EditText et_search;
    private Button btn_search;
    private FirebaseFirestore database;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        et_search = findViewById(R.id.et_username);
        btn_search = findViewById(R.id.bn_searchuser);
        database =FirebaseFirestore.getInstance();
        btn_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (et_search.getText().toString().isEmpty()) {
                    Toast toast = Toast.makeText(getApplicationContext(), "Selecciona el usuario", Toast.LENGTH_LONG);
                    toast.show();
                }else{
                    WaitDialog.showWait(SearchActivity.this, "Buscando...");
                    CollectionReference ref = database.collection("usuarios");
                    ref.get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            if (task.isSuccessful()){
                                boolean existe = false;
                                for (QueryDocumentSnapshot doc: task.getResult()){
                                    User user = doc.toObject(User.class);
                                    if (user.getIdUser().equals(et_search.getText().toString())){
                                        existe = true;
                                        break;
                                    }
                                }
                                if (existe){
                                    Constants.userSearch = et_search.getText().toString();
                                    tabLayout = findViewById(R.id.tabLayoutUsuarios);
                                    viewPager = findViewById(R.id.viewPagerUsuarios);
                                    tabLayout.setupWithViewPager(viewPager);
                                    viewPager.setAdapter(new SearchAdapter(getSupportFragmentManager()));
                                }else{
                                    Toast toast = Toast.makeText(getApplicationContext(), "El usuario noe existe", Toast.LENGTH_LONG);
                                    toast.show();
                                }
                                if (WaitDialog.alertDialog!=null){
                                    WaitDialog.closeWait();
                                }
                            }
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast toast = Toast.makeText(getApplicationContext(), "Ha ocurrido un error", Toast.LENGTH_LONG);
                            toast.show();
                        }
                    });

                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(getApplicationContext(), MainActivity.class));
        super.onBackPressed();
    }
}
