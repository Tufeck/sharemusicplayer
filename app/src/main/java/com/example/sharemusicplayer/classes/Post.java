package com.example.sharemusicplayer.classes;


public class Post {

    private String idUser;
    private String post;
    private String type;
    private String date;
    private String postId;
    private String musicName;
    private String musciDuration;
    private String dataMusic;


    public Post() {
    }

    public Post(String idUser, String post, String type, String date, String nombreMusic, String musciDuration, String dataMusic) {
        this.idUser = idUser;
        this.post = post;
        this.type = type;
        this.date = date;
        this.musicName = nombreMusic;
        this.musciDuration = musciDuration;
        this.dataMusic = dataMusic;
    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getNombreMusic() {
        return musicName;
    }

    public void setNombreMusic(String nombreMusic) {
        this.musicName = nombreMusic;
    }

    public String getMusciDuration() {
        return musciDuration;
    }

    public void setMusciDuration(String musciDuration) {
        this.musciDuration = musciDuration;
    }

    public String getDataMusic() {
        return dataMusic;
    }

    public void setDataMusic(String dataMusic) {
        this.dataMusic = dataMusic;
    }

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }
}
